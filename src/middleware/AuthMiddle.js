const jwt = require('jsonwebtoken')
const key = require('../config/key')

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization

    //verify token
    if (!authHeader)
        return res.status(401).send({ error: 'token invalid' });
    const parts = authHeader.split(" ")
    if (!parts.length === 2)
        return res.status(401).send({ error: 'token-error' });
    const [scheme, token] = parts
    if (!/^Bearer$/i.test(scheme))
        return res.status(401).send({ error: 'token malformatted' });
    jwt.verify(token, key, (err, decode) => {
        if (err) return res.status(401).send({ error: 'error in token' });
        req.idUser = decode.id
        return next()

    })
}

