const mongoose = require('mongoose')

const ServiceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    slug: {
        type: String,
    },
    solicitacoes: [{
        type: Number

    }],
    lancamento: {
        type: Date,
    },
    stars: {
        type: Number,
        default: 1
    },
    idUserCreate:{
        type:Number,
        required:true
    },
    dataCreate: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model('Servico', ServiceSchema)