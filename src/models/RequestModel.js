const mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId;
const RequestSchema = new mongoose.Schema({
    idUser: {
        type: String,
        required: true
    },
    idService: {
        type: ObjectId,
        required: true
    },
    dataRequest: {
        type: Date,
        default: Date.now
    }


})

module.exports = mongoose.model('Solicitacoes', RequestSchema)