const express = require('express')

const AuthMiddle = require('./middleware/AuthMiddle')
const ServiceController = require('./controllers/ServiceController')
const RequestController = require('./controllers/RequestController')

const routes = express.Router()

routes.get('/servico', ServiceController.index)
routes.post('/servico', ServiceController.store)
routes.post('/servico/:id', ServiceController.show)
routes.get('/solicitacao', AuthMiddle, RequestController.index)
routes.post('/solicitacao', AuthMiddle, RequestController.store)


module.exports = routes