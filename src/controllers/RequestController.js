const Solicitacao = require('../models/RequestModel')
const Servico = require('../models/serviceModel')

module.exports = {
    async index(req, res) {
        try {
            //get all solicitacoes
            const request = await Solicitacao.find()
            res.status(200).send({ request })
        } catch (err) {
            res.status(400).send({ error: 'failed get request' })
        }
    },
    async store(req, res) {

        const { idService, idUser } = req.body

        try {

            if (await Servico.findOne({ _id: idService }) === null)
                return res.status(404).send({ error: 'service not exist' })
            //check likes
            if (await Solicitacao.findOne({
                $and: [
                    { idService },
                    { idUser }
                ]
            }))
                return res.status(403).send({ error: 'dont like' })

            //search idService for add like, before add and save
            const service = await Servico.findById(idService)
            service.stars += 1
            service.solicitacoes.push(idUser)
            await service.save()

            req.io.emit('NewSolic', service)
            //Save data
            const request = await Solicitacao.create(req.body)

            res.status(201).send({ request })

        } catch (err) {
            return res.status(200).send({ error: "failed store request" })
        }

    }
}