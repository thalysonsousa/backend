const jwt = require('jsonwebtoken')

const Servico = require('../models/serviceModel')
const Solicitacao = require('../models/RequestModel')
const key = require('../config/key')

//generate token 
const genToken = (param = {}) => {
    return jwt.sign(param, key, {
        expiresIn: '1h'
    })
}

module.exports = {
    async index(req, res) {
        try {
            const solic = await Solicitacao.find({ idUser: 2 })

            //procura todos os servicos 
            const serv = await Servico.find().sort({stars:-1})
            //example gen token
            res.status(200).send({ serv, token: genToken({ id: '2' }) })
        } catch (err) {
            res.status(400).send({ error: 'failed get service' })
        }
    },
    async show(req, res) {
        try {
            const id = req.params.id
            if (await Servico.findById(id) === null)
                return res.status(404).send({ error: 'service not found' })
            const serv = await Servico.findById(id)
            res.status(200).send(serv)
        } catch (err) {
            res.status(400).send({ error: 'failed show service' })
        }

    },
    async store(req, res) {

        const { idUserCreate: idUser, _id, name, slug } = req.body
        
            const serv = await Servico.create({
                name,
                slug,
                idUserCreate:idUser,
                solicitacoes: idUser
            })
            req.io.emit('NewServ', serv)
            await Solicitacao.create({
                idUser:serv.idUserCreate,
                idService:serv._id
            })
            
            res.status(201).send({ serv })
            console.log(req.body)
        

    }
}